---
title: Privacy
description: Find here the privacy of R2Devops
---

## We respect you
R2Devops does not collect your personal data! We do track the visits on our website, but by using [Plausible.io](https://plausible.io/data-policy){:target="_blank"}, a GDPR compliant solution, we ensure no personal data are stored. With Plausible, we don't use cookies and respect the privacy of our website visitors.

## Privacy Policy
R2Devops’s Privacy Policy details the different ways personal data received from users are collected via the Website.

!!! success "It's simple, we do not store any personal data except your GitLab username if you are a contributor."


## Personal data management
In France, personal data is notably protected by law n ° 78-87 of January 6, 1978, law n ° 2004-801 of August 6, 2004, article L. 226-13 of the Criminal Code and the European Directive of October 24, 1995.
On the occasion of the use of the site www.r2devops.io, can be collected: the URL of the links through which the user has accessed the site www.r2devops.io, the access provider of the user, the Internet Protocol (IP) address of the user.
The site is not declared to the CNIL because it does not collect personal information.
There is no database.
The data are stored in the GitLab repository. The website is exposed in the static GitLab pages.

## Hypertext links and cookies
The www.r2devops.io site contains a number of hypertext links to other sites, set up with the authorization of Go2Scale. However, Go2Scale does not have the possibility of verifying the content of the sites thus visited, and will not therefore assume any responsibility for this fact.
     Browsing the www.r2devops.io site may cause the installation of cookie(s) on the user’s computer. A cookie is a small file, which does not allow the identification of the user, but which records information relating to the navigation of a computer on a site. The data thus obtained are intended to facilitate subsequent navigation on the site, and are also intended to allow various measures of attendance. Those data will allow the accounting of interactions on articles (comments and likes). Refusal to install a cookie may make it impossible to access certain services. The user can however configure his computer as follows, to refuse the installation of cookies:     

– Under Internet Explorer: tool tab (cog-shaped pictogram in the upper right) / internet options. Click on Confidentiality and choose Block all cookies. Validate on Ok.

– In Firefox: at the top of the browser window, click on the Firefox button, then go to the Options tab. Click on the Privacy tab.        Configure the Storage rules on: use personalized parameters for history. Finally uncheck it to deactivate cookies.     

– In Safari: Click on the menu icon at the top right of the browser (symbolized by a cog). Select Settings. Click on Show advanced settings. In the “Confidentiality” section, click on Content settings. In the “Cookies” section, you can block cookies.

– Under Chrome: Click on the menu icon at the top right of the browser (symbolized by three horizontal lines). Select Settings. Click on Show advanced settings. In the “Confidentiality” section, click on preferences. In the “Confidentiality” tab, you can block cookies.  In the “Confidentiality” section, click on preferences. In the “Confidentiality” tab, you can block cookies.

We use Hotjar in order to better understand our users’ needs and to optimize this service and experience. Hotjar is a technology service that helps us better understand our users’ experience (e.g. how much time they spend on which pages, which links they choose to click, what users do and don’t like, etc.) and this enables us to build and maintain our service with user feedback. Hotjar uses cookies and other technologies to collect data on our users’ behavior and their devices. This includes a device’s IP address (processed during your session and stored in a de-identified form), device screen size, device type (unique device identifiers), browser information, geographic location (country only), and the preferred language used to display our website. Hotjar stores this information on our behalf in a pseudonymized user profile. Hotjar is contractually forbidden to sell any of the data collected on our behalf.
For further details, please see the ‘about Hotjar’ section of  [Hotjar’s support site.](https://help.hotjar.com/hc/en-us/categories/115001323967-About-Hotjar)


--8<-- "includes/abbreviations.md"
